FROM python:alpine
COPY [".", "/app"]
WORKDIR /app
RUN apk add --update musl-dev gcc libffi-dev postgresql-dev python3-dev
# TODO: Improve the Dockerfile for different environments
RUN pip install pipenv && pipenv sync
ENV FLASK_APP="entrypoint:app"
CMD ["pipenv", "run", "flask", "run", "--host", "0.0.0.0"]