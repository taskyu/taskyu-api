from functools import wraps
from flask import request, abort, Response, jsonify, g
from app.utils.jwt import validate_token

def is_auth(func):
  @wraps(func)
  def _is_auth(*args, **kwargs):
    authorization = request.headers.get('Authorization')
    token_validation = validate_token(authorization)
    g.token_payload = token_validation
    return func(*args, **kwargs)
  return _is_auth
