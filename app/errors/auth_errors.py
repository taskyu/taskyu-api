from flask import jsonify

class AuthError(Exception):
  pass

class PermissionError(Exception):
  pass

def register_auth_error_handlers(app):
  @app.errorhandler(AuthError)
  def handle_auth_error(e):
    return jsonify({'msg': str(e)}), 401

  @app.errorhandler(PermissionError)
  def handle_permission_error(e):
    return jsonify({'msg': str(e) or 'NOT_PERMISSION'}), 403