import os
from flask import jsonify

class AppErrorBaseClass(Exception):
  pass

class ObjectNotFound(AppErrorBaseClass):
  pass

def register_base_error_handlers(app):
  @app.errorhandler(Exception)
  def handle_exception_error(e):
    print(e)
    return jsonify({'msg': 'Internal server errors' if os.getenv('production') else str(e)}), 500

  @app.errorhandler(405)
  def handle_405_error(e):
    return jsonify({'msg': 'Method not allowed'}), 405

  @app.errorhandler(403)
  def handle_403_error(e):
    return jsonify({'msg': 'Forbidden error'}), 403

  @app.errorhandler(404)
  def handle_404_error(e):
    return jsonify({'msg': 'Not Found error'}), 404

  @app.errorhandler(AppErrorBaseClass)
  def handle_app_base_error(e):
    print(e)
    return jsonify({'msg': str(e)}), 500

  @app.errorhandler(ObjectNotFound)
  def handle_object_not_found_error(e):
    return jsonify({'msg': str(e)}), 404