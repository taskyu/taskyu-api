from flask import jsonify
from sqlalchemy.exc import IntegrityError
from marshmallow import ValidationError
  
def register_ext_error_handlers(app):
  @app.errorhandler(IntegrityError)
  def handle_integrity_error(e):
    print(e.code)
    return jsonify({'msg': str(e)}), 409

  @app.errorhandler(ValidationError)
  def handle_validation_error(e):
    return jsonify({'msg': str(e)}), 400