from flask import jsonify

class InvalidToken(Exception):
  pass

def register_jwt_error_handlers(app):
  @app.errorhandler(InvalidToken)
  def handle_invalid_token_error(e):
    return jsonify({'msg': str(e)}), 403