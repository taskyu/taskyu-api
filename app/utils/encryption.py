import bcrypt

def encrypt_password(password):
  return bcrypt.hashpw(
    password.encode('utf-8'),
    bcrypt.gensalt()
  ).decode('utf-8')

def validate_password(password, password_from_db):
  return bcrypt.checkpw(
    password.encode('utf-8'),
    password_from_db.encode('utf-8')
  )
