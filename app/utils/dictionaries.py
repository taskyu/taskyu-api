def omit_keys(dictionary, keys):
  return { key: value for key, value in dictionary.items() if key not in keys }