import os, time, jwt
from app.errors import InvalidToken
from app.utils.dictionaries import omit_keys

def generate_token(payload, exp_minutes = 1440, sub_field='id', omit = ['id', 'password', 'createdAt', 'updatedAt', 'birth']):
  base_payload = {
    'exp' : int(time.time()) + (60*exp_minutes), 
    'iat' : int(time.time()),
    'sub' : payload.get(sub_field)
  }
  base_payload.update(omit_keys(payload, omit))
  return jwt.encode(base_payload, os.getenv('SECRET_KEY'), algorithm='HS256').decode('utf-8')

def validate_token(authorization, sub = None):
  try:
    split_auth = authorization.split(' ')
    if split_auth[0] != 'Bearer':
      raise InvalidToken('NO_BEARER_TOKEN')
    decoded_token = jwt.decode(split_auth[1], os.getenv('SECRET_KEY'), algorithm='HS256')
  except: 
    raise InvalidToken('INVALID_TOKEN')
  if decoded_token['exp'] < time.time():
    raise InvalidToken('EXPIRED_TOKEN')
  elif sub != None and decoded_token['sub'] != sub:
    raise InvalidToken('INVALID_SUB')
  else:
    return decoded_token
