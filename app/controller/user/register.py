from . import user_schema
from app.models import UserEntity

def register(user_json):
  user_dict = user_schema.load(user_json)
  user = UserEntity(email=user_dict.get('email'),
              firstname=user_dict.get('firstname'),
              lastname=user_dict.get('lastname'),
              birth=user_dict.get('birth'),
              password=user_dict.get('password')
  )
  user.password_encrypt()
  user.save()
  resp = user_schema.dump(user)
  return resp, 201