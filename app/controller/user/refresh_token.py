from flask import g
from app.models import UserEntity
from app.errors import ObjectNotFound, PermissionError
from . import user_schema
from app.utils.jwt import generate_token

def refresh_token():
  uid = g.token_payload.get('sub')
  user = UserEntity.get_by_id(uid)
  if user is None:
    raise ObjectNotFound('USER_NOT_EXISTS')
  user_data = user_schema.dump(user)
  resp = { 'accessToken': generate_token(user_data) }
  return resp, 200