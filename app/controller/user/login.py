from app.models import UserLoginSchema, UserEntity
from . import user_schema
from app.utils.jwt import generate_token
from app.errors import AuthError

login_schema = UserLoginSchema()

def login(login_json):
  login_dict = login_schema.load(login_json)
  login_user = UserEntity.filter_first(email=login_dict['email'])
  if not login_user:
    raise AuthError('WRONG_CREDENTIALS')
  login_user.password_validate(login_dict['password'])
  user_data = user_schema.dump(login_user)
  resp = { 'accessToken': generate_token(user_data) }
  return resp, 200