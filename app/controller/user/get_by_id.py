from flask import g
from app.models import UserEntity
from app.errors import ObjectNotFound, PermissionError
from . import user_schema

def get_by_id(user_id):
  uid_from_token = g.token_payload.get('sub')
  if user_id != uid_from_token:
    raise PermissionError
  user = UserEntity.get_by_id(user_id)
  if user is None:
    raise ObjectNotFound('USER_NOT_EXISTS')
  res = user_schema.dump(user)
  return res