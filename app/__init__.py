from flask import Flask, jsonify
from flask_restful import Api
from flask_cors import CORS
from .db import db
from .routes import users_bp
from .ext import ma, migrate
from .errors import register_base_error_handlers, register_auth_error_handlers, register_ext_error_handlers, register_jwt_error_handlers
from .converters import RegexConverter

def create_app(settings_module):
  app = Flask(__name__)
  app.config.from_object(settings_module)

  # Inicializa las extensiones
  CORS(app)
  db.init_app(app)
  ma.init_app(app)
  migrate.init_app(app, db)

  # Captura todos los errores 404
  Api(app, catch_all_404s=True)

  # Deshabilita el modo estricto de acabado de una URL con /
  app.url_map.strict_slashes = False

  # Añade custom converters
  app.url_map.converters['regex'] = RegexConverter

  # Registra los blueprints
  app.register_blueprint(users_bp)

  # Registra manejadores de errores personalizados
  register_base_error_handlers(app)
  register_auth_error_handlers(app)
  register_ext_error_handlers(app)
  register_jwt_error_handlers(app)

  return app
