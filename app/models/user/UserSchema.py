from marshmallow import fields
from app.ext import ma

class UserSchema(ma.Schema):
  id = fields.UUID()
  email =  fields.Email(required=True)
  firstname = fields.String(required=True)
  lastname = fields.String(required=True)
  birth = fields.Date()
  password = fields.String()
  validated = fields.Boolean(allow_none=True)
  createdAt = fields.Integer(allow_none=True)
  updatedAt = fields.Integer(allow_none=True)

class UserLoginSchema(ma.Schema):
  email =  fields.Email(required=True)
  password = fields.String(required=True)