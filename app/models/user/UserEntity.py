from app.db import db, BaseModelMixin
from ..base import BaseEntity
from app.utils.encryption import encrypt_password, validate_password
from app.errors import AuthError

class UserEntity(BaseEntity, db.Model, BaseModelMixin):
  __tablename__ = 'user'
  email = db.Column(db.String(60), nullable=False, unique=True)
  firstname = db.Column(db.String(50), nullable=False)
  lastname = db.Column(db.String(50), nullable=False)
  birth = db.Column(db.Date(), nullable=False)  # YYYY-MM-DD
  password = db.Column(db.String(72), nullable=False)
  validated = db.Column(db.Boolean(), default=True) # TODO: Switch to False when email validation implemented

  def password_encrypt(self):
    self.password = encrypt_password(self.password)
  
  def password_validate(self, password):
    if validate_password(password, self.password):
      pass
    else:
      raise AuthError('WRONG_CREDENTIALS')

  def __repr__(self):
    return '<User %r>' % self.email