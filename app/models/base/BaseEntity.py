import time
import uuid
from app.db import db
from sqlalchemy.dialects.postgresql import UUID

class BaseEntity(db.Model):
  __abstract__ = True
  id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
  createdAt = db.Column(db.Integer, default=time.time())
  updatedAt = db.Column(db.Integer, default=time.time())