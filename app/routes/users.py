from flask import request, Blueprint
from flask_restful import Api, Resource
from app.models import UserSchema, UserEntity
from app.middleware import is_auth
from app.controller.user import get_by_id, register, login, refresh_token
from app.constants.regex import UUID_REGEX

users_bp = Blueprint('users', __name__)
user_schema = UserSchema()

class UserRegisterResource(Resource):
  def post(self):
    data = request.get_json()
    return register(data)

class UserLoginResource(Resource):
  def post(self):
    data = request.get_json()
    return login(data)

class UserByIdResource(Resource):
  @is_auth
  def get(self, user_id):
    return get_by_id(user_id)

class UserTokenRefresh(Resource):
  @is_auth
  def get(self):
    return refresh_token()


api = Api(users_bp)
api.add_resource(UserRegisterResource, '/user/register', endpoint='user_register_resource')
api.add_resource(UserLoginResource, '/user/login', endpoint='user_login_resource')
api.add_resource(UserTokenRefresh, '/user/refresh', endpoint='user_refresh_resource')
api.add_resource(UserByIdResource, f'/user/<regex("{UUID_REGEX}"):user_id>', endpoint='user_by_id_resource')

