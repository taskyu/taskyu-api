Taskyu API
---

API para la aplicación Taskyu

**Version**: 0.1.2

## Run the project

Setup .env file before execute, based on .env.example file

### Docker

```bash
docker-compose up -d

docker-compose exec flask pipenv run flask db upgrade

# API Running on PORT 5001
```

### Pipenv

```bash
pipenv sync

pipenv run flask db upgrade

pipenv run flask run --host 0.0.0.0

# API Running on PORT 5000
```
